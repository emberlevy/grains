import mock
import pytest


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="tool")
    hub.pop.sub.load_subdirs(hub.tool, recurse=True)
    hub.pop.sub.add(dyne_name="exec")
    hub.pop.sub.add(dyne_name="states")
    hub.pop.sub.add(dyne_name="grains")

    with mock.patch("sys.argv", ["grains"]):
        hub.pop.config.load(["grains", "idem", "rend"], "grains", parse_cli=True)

    hub.grains.init.standalone()

    yield hub
